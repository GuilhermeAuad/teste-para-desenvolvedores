## Referências Técnicas

1. Quais foram os últimos dois livros técnicos que você leu?
Descobrindo o Linux e Análise de tráfego em redes TCP/IP.

2. Quais foram os últimos dois framework/CMS que você trabalhou?
Zope/Plone e Joomla.

3. Descreva os principais pontos positivos do seu framework favorito.
Joomla tem uma facilidade maior para aprender e encontrar documentação. Também é bem mais flexível em relação ao Plone.

4. Descreva os principais pontos negativos do seu framework favorito.
Tem um controle menor que o Plone, podendo em alguns casos ser um pouco confuso encontrar onde eles estão. Devido a maior flexibilidade e facilidade em fazer alguns “gatos” em alguns casos, alterações de clientes curiosos podem “estragar” o site.

5. O que é código de qualidade para você.
Um código legível que pode ser entendido, alterado e mantido facilmente por qualquer desenvolvedor.

## Conhecimento Linux

1. O que é software livre?
Software livre tem haver a liberdade e não de preço. Refere-se à liberdade dos usuários de: executar, copiar, distribuir, estudar, modificar e aperfeiçoar o software.

2. Qual o seu sistema operacional favorito?
Depende. Windows tem algumas facilidades que o Linux ainda não oferece. Em contra partida o Debian, que costumo utilizar, tem algumas ferramentas poderosas além da maioria dos aplicativos serem gratuitos.

3. Já trabalhou com Linux ou outro Unix-like?
Sim, Debian, Ubuntu e Kali linux.

4. O que é SSH?
Security Shell é um protocolo que fornece uma conexão segura para uma máquina remota oferecendo uma janela Shell que executa os comandos nesta máquina.

5. Quais as principais diferenças entre sistemas *nix e o Windows?
Os sistemas *nix têm código aberto, que te permite estudá-lo e alterá-lo da maneira que quiser. 
Outra diferença é o modo como os dados são tratados. Por exemplo, o Windos não é case sensitive ao contrário dos sistemas Unix-like.


## Conhecimento de desenvolvimento

1. O que é GIT?
É um software de controle de versão. Ele permite gerenciar e guardar um histórico das modificações de um conjunto códigos.

2. Descreva um workflow simples de trabalho utilizando GIT.
Em uma situação onde exista uma aplicação já em produção que precisa ser corrigida, deve-se baixar a versão que está em produção(checkout v1.0) ; fazer um branch para consertar o bug (branch abc); e novamente subir a produção(merge abc).

3. O que é PHP Data Objects?
É uma interface que define um conjunto de classes e a assinatura dos métodos de comunicação com uma base de dados.

4. O que é Database Abstract Layer?
É uma interface que unifica a comunicação entre as aplicações e diferentes bancos de dados.

5. Você sabe o que é Object Relational Mapping? Se sim, explique.
É uma técnica de mapeamento de tabelas de um banco de dados relacional para objetos.

6. Como você avalia seu grau de conhecimento em Orientação a objeto?
Bom.

7. O que é Dependency Injection?
É um padrão de desenvolvimento onde as dependências dos módulos de uma aplicação são modificadas em tempo de execução. 

8. O significa a sigla S.O.L.I.D?
São 5 princípios para programação orientada a objeto: Responsabilidade única, Open closed, Substituição, Segregação de interfaces, Inversão de dependências.

9. Qual a finalidade do framework PHPUnit?
É um framework que auxilia a fazer testes unitários em PHP.

10. Explique o que é MVC.
É um modelo de arquitetura de software que separa a representação da informação(model) da interação do usuário(view).
Na web é muito comum, principalmente quando se utiliza CMS, onde existe um gerenciador de conteúdo que controla a visão dos dados em diferentes situações.